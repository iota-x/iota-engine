module.exports = {
  // Node to connect to, suggest to setup your own node to have full control
  // For suggestions on public servers see http://iotasupport.com/lightwallet.shtml
  'provider': 'http://service.iotasupport.com:14265',
  // Security Level of the Addresses created
  // Only change this if you know what you are doing. Available choices are 1, 2 and 3.
  // If you dont see your balance anymore, thats ok, just switch back to see it again.
  addressSecurityLevel: 2,
  tag: 'SENT9BY9IOTA9ENGINE99999999',
  message: 'sent with iota engine',
  // Depth for tip selection algo
  depth: 5,
  minWeightMagnitude: 14
}
