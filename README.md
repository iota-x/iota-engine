####  **NOTE**:  iota engine is a package in development - As such, there might be some unexpected results. Please join the community (see links below) and post issues and enhancements on here, to ensure that we can improve the usability - feel free to write to __@instantlink99__ or __@isarstyle__ in [iota slack channel](https://iotatangle.slack.com) for support

See a demo web app built with the help of iota-engine

[iota engine demo app](http://iota-engine.tech)


# IOTA Engine Javascript Library

## Summary
---

### IOTA engine provides you with a frame of methods and functions in a more natural fashion by hiding much of the deeper lying IOTA framework

As we found it difficult for developers to start with writing IOTA Applications, we created this library for developers to simplify building apps using the IOTA Tangle Network.

Currently main focus is to simplify pow (iota's proof of work) and provide a client-server architecture for your apps to easily let run the pow on the client

Thus you can use this package any nodeJS environment you wish to: If you have webgl2 enabled you can profit from your client-side gpu acceleration. Otherwise it will use local pow (CPU)

---

# Installation

``` bash

# install with npm
$ npm install --save iota-engine

```
---
``` js
// Import into your project

const iotaEngine = require('iota-engine')

```
---
# Usage

## Demo Usage for sending iota on a client-server architecture where you can store the seed safely on the server and do pow on the client

``` js
var iotaEngine = require('iota-engine')

//on the server
iotaEngine.initServer(seed)
iotaEngine.createBundle(amount, address)

// on the client
iotaEngine.initClient()
iotaEngine.attachBundle(serverBundle) // put in bundle created on the server and attach to Tangle

```

``` bash

If you want to modify for your own specific setup, please checkout
the project and read iota-engine.js to see all implemented methods,
which may help you on how to properly use attachToTangle method


What can the IOTA Engine currently do for you:

 - initClient
 - initServer

 - getBalance

 - createAddresses

 - createBundle
 - isCorrectBundle
 - attachBundle

 (old sendIOTA method which works client-side only
 ... needs seed on client, might be security risk)
 - sendIOTA

 # in development
 ```


If you crave for help or for a certain functionality please let us know!
---

 Keep the IOTA engine running, fuel it with IOTA ♥: ``` ZSMTNIACPEHZOSETNKJFWZDYUHPUWJWYQCGUQRMUFPQWQKTMVPPXPOWQNQPNRRJ9ONBKMPD9WIVUUJMCZQJFCXOJ9C ```
